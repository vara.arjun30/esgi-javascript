	function ucfirst(str) {

		if(typeof str !== "string" | !str) return '';

		return str[0].toUpperCase() + str.substring(1);
	}

	console.log(ucfirst("test"));
	console.log(ucfirst("Test"));
	console.log(ucfirst(" test"));
	console.log(ucfirst("test Test tst"));
	console.log(ucfirst(""));
	console.log(ucfirst("null"));
	console.log(ucfirst(({}));

	function capitalize(str) {
		return str.toLowerCase().split(" ").map(function(item) {
			return ucfirst(item);
		}).join(" ");
	}

	console.log(ucfirst("test"));
	console.log(ucfirst("Test"));
	console.log(ucfirst(" test"));
	console.log(ucfirst("test Test tst"));
	console.log(ucfirst(""));
	console.log(ucfirst("null"));
	console.log(ucfirst(({}));